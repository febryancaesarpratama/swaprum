class ResponseCode {

    successGet(req,res, message,data){
        return res.status(200).json({
            status: true,
            message,
            data
        })
    }

    successPost(req,res, message){
        return res.status(201).json({
            status: true,
            message
        })
    }

    errorValidation(req,res, message){
        return res.status(422).json({
            status: false,
            message
        })
    }

    errorGet(req,res, message){
        return res.status(404).json({
            status: false,
            message
        })
    }  

    errorPost(req,res, message){
        return res.status(400).json({
            status: false,
            message
        })
    }

    errorAuth(req,res, message){
        return res.status(401).json({
            status: false,
            message
        })
    }
    
}

module.exports = new ResponseCode();