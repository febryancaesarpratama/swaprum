const { default: axios } = require("axios");
const { response } = require("express");
const db = require("../../../../database/models");

class SwaprumController{


    async getHistory(req,res){
        let response = await db.history.findAll();
        
        return res.status(200).json({message: response});
    }

    async getSwaprum(req, res){

        // return res.status(200).json({message: "Let's Show The World."});

//         let response = await axios.get('https://swaprum.finance/server/free-token?address=0x1105c88EeF4c249d05b05D54FA9887a09478d074',{
//     headers: {
//       'Content-Type': 'application/json',
//       'Access-Control-Allow-Origin': '*',
//       'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36 Edg/113.0.1774.42',
//       'address': "0x1105c88EeF4c249d05b05D54FA9887a09478d074"
//     }
//  });

//         return res.status(200).json({message: response.data})
        let response = await axios.get('https://swaprum.finance/server/claim-free?address=0x1105c88EeF4c249d05b05D54FA9887a09478d074',{
            headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36 Edg/113.0.1774.42',
            'address': "0x1105c88EeF4c249d05b05D54FA9887a09478d074"
            }
        });

        
        if (response.data.error == 'wait 1 hour'){
            await db.history.create({
                // address: "0x1105c88EeF4c249d05b05D54FA9887a09478d074",
                keterangan: "wait 1 hour",
                amount: 0
            })
            return res.status(200).json({message: response.data.error})
        }else{
            await db.history.create({
                // address: "0x1105c88EeF4c249d05b05D54FA9887a09478d074",
                keterangan: "Successfully Claimed",
                amount: response.data.amount
            })

            return res.status(200).json({message: "Successfully Claim"})
        }
    }

}

module.exports = new SwaprumController();