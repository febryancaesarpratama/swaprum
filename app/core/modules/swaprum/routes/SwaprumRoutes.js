
const router = require("express").Router();
const SwaprumController = require("../controllers/SwaprumController");

router.get(
    '/swaprum',
    SwaprumController.getSwaprum
);

router.get(
    '/history',
    SwaprumController.getHistory
)

module.exports = router;