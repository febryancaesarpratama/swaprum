module.exports = (sequelize, Sequelize) => {
  const History = sequelize.define("histories", {
    keterangan:{
        type: Sequelize.STRING,
        allowNull: false
    },
    amount:{
        type: Sequelize.INTEGER,
        allowNull: true
    },
    createdAt: {
      type: Sequelize.DATE,
      allowNull: false,
    },
    updatedAt: {
      type: Sequelize.DATE,
      allowNull: false,
    },
  });
  return History;
};
