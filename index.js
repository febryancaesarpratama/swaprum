const express = require('express');
const cors = require('cors');
const app = express();
const port = 3000;
const bodyParser = require('body-parser');

const SwaprumRoutes = require('./app/core/modules/swaprum/routes/SwaprumRoutes');

app.use(cors());
app.use(bodyParser.json({limit: '50mb', type: 'application/json'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true, parameterLimit: 50000}));


const db = require('./app/database/models');

db.sequelize.sync({force: false}).then(() => {
    console.log("Drop and re-sync db.");
});

app.get('/', (req, res) => {
    res.json({message: "Let's Show The World."});
});

app.use('/api', SwaprumRoutes);


app.listen(port, () => 
    console.log(`Example app listening at http://localhost:${port}`)
);